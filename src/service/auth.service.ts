import { hash, compare } from "bcrypt";
import {
    InternalServerError,
    BadRequestError,
    UnauthorizedError,
    ForbiddenError,
} from "routing-controllers";
import { Service } from "typedi";
import { Repository } from "typeorm";
import { OrmRepository } from "typeorm-typedi-extensions";
import { stripCredentials, permissionLevels } from "../config";
import { generateAccessToken } from "../controllers/auth.controller";
import { User } from "../database/entities/user";
import jwt from "jsonwebtoken";

@Service()
export class AuthService {
    refreshTokens: string[] = [];

    @OrmRepository(User)
    repo: Repository<User>;

    async createUser(username: string, name: string, password: string) {
        try {
            const hashedPassword = await hash(password, 10);
            await this.repo.insert({
                username,
                name,
                password: hashedPassword,
                // providers: [],
                roles: [permissionLevels.user],
            });
            const user = await this.repo.findOne({ username });
            return { user: stripCredentials(user) };
        } catch (err) {
            throw new InternalServerError(err.toString());
        }
    }

    async login(username: string, password: string) {
        let user = await this.repo.findOne({ username });
        if (!user) throw new BadRequestError(`Cannot find user ${username}`);
        if (!(await compare(password, user.password)))
            throw new UnauthorizedError("Invalid credentials");

        const accessToken = generateAccessToken(stripCredentials(user));
        const refreshToken = jwt.sign(
            stripCredentials(user),
            process.env.REFRESH_TOKEN_SECRET,
        );
        this.refreshTokens.push(refreshToken);
        return { accessToken, refreshToken };
    }

    refreshToken(token: string) {
        if (token === null)
            throw new UnauthorizedError("No refresh token specified");
        if (!this.refreshTokens.includes(token))
            throw new ForbiddenError("Invalid refresh token");
        try {
            const user = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
            const accessToken = generateAccessToken({
                username: (user as User).username,
            });
            return { accessToken };
        } catch (err) {
            throw new ForbiddenError(err.toString());
        }
    }

    logout(bodyToken: string) {
        this.refreshTokens = this.refreshTokens.filter(
            (token) => token !== bodyToken,
        );
        return { message: "Logged out successfully." };
    }
}
