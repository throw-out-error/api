import Stripe from "stripe";
import dotenv from "dotenv";
import { Request, Response, NextFunction } from "express";
import { User } from "./database/entities/user";

const result = dotenv.config();
if (result.error) {
    console.log(`Failed to parse .env file`);
} else {
    process.env = result.parsed || process.env;
}

export const debug = process.env.DEBUG_LOG;

export const dbUrl =
    process.env.MONGO_URI || "mongodb://localhost:27017/website";
export const permissionLevels = {
    user: "user",
    writer: "writer",
    admin: "admin",
};
export const saltRounds = 10;
/* 
const PostSchema = new Schema({
    title: { type: String, required: true },
    content: { type: String, required: true },
    createdAt: { type: Number, required: true },
    author: { type: String, required: true },
    category: { type: String, required: false },
    description: { type: String, required: true },
});

const ProjectSchema = new Schema({
    url: { type: String, required: false },
    codeUrl: { type: String, required: false },
    name: { type: String, required: true },
    description: { type: String, required: true },
}); */

export const stripCredentials = (user: User) => {
    delete user.password;
    return JSON.parse(JSON.stringify(user));
};

/**
 * Assumes the user is already logged in and their session is valid
 */
export const hasRole = async (req: Request, role): Promise<boolean> => {
    const user = req.session.user;
    if (!user) return false;
    return user.roles.includes(role);
};

/* // TODO; make this optional
export const stripe = new Stripe(process.env.stripeKey, { apiVersion: '2020-03-02' })

export const getProducts = async () => {
  const products = await stripe.products.list({ limit: 100 })
  const productList = []
  for(const [index, p] of products.data.entries()){
    const product = {
      ...p,
      skus: (await stripe.skus.list({ product: p.id })).data,
    }
    productList.push(product)
  }
  return productList
}
 */
