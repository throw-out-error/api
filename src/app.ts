// Import dependencies
import express, { Request } from "express";
import flash from "express-flash";
import session from "express-session";
import { createStream } from "rotating-file-stream";
import rateLimit from "express-rate-limit";
import "reflect-metadata";
import {
    Action,
    BadRequestError,
    createExpressServer,
    UnauthorizedError,
} from "routing-controllers";
import jwt from "jsonwebtoken";
import { AuthController } from "./controllers/auth.controller";
import path from "path";
import { CustomErrorHandler } from "./controllers/error.middleware";
import winston from "winston";
import expressWinston from "express-winston";
import { User } from "./database/entities/user";
import { IndexController } from "./controllers/index.controller";
import { BlogController } from "./controllers/blog.controller";

export const verifyToken = (token: string): User => {
    if (!token) throw new UnauthorizedError("No access token provided.");
    try {
        const user = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
        return user as User;
    } catch (err) {
        throw new BadRequestError("Invalid access token");
    }
};

// Create http server using express
export const app: express.Application = createExpressServer({
    controllers: [AuthController, BlogController, IndexController],
    middlewares: [CustomErrorHandler],
    cors: true,
    defaultErrorHandler: false,
    authorizationChecker: async (action: Action, roles: string[]) => {
        const token = (action.request as Request).headers[
            "authorization"
        ] as string;

        const user = verifyToken(token);

        if (user) action.request.user = user;

        if (user && !roles.length) return true;
        if (user && roles.find((role) => user.roles.indexOf(role) !== -1))
            return true;

        return false;
    },
    currentUserChecker: async (action: Action) => {
        return action.request.user;
    },
});

/* 
If we don't use this, accessing the api from the frontend
will give us a cross origin (cors) error because they are on different ports.
Also has to be one of the first routes in order for it to work.
*/
const allowedOrigins: string[] = [
    "localhost:8080",
    "localhost:3000",
    "theoparis.com",
    "api.theoparis.com",
];
/* app.use(
    cors({
        credentials: true,
        origin: "*",
    }),
); */
app.use(
    session({
        secret: "12 34",
        resave: true,
        saveUninitialized: true,
    }),
);
app.use(
    rateLimit({
        windowMs: 60 * 1000,
        max: 30, // limit each IP to n requests per windowMs
    }),
);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(flash());
app.disable("etag");

const logDir = path.join(__dirname, "..", "log");

const accessLogStream = createStream("access.log", {
    interval: "1d", // rotate daily
    path: logDir,
});

const errorLogStream = createStream("error.log", {
    interval: "1d", // rotate daily
    path: logDir,
});

app.use(
    expressWinston.logger({
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.json(),
        ),
        transports: [
            //
            // - Write all logs with level `error` and below to `error.log`
            // - Write all logs with level `info` and below to `combined.log`
            //
            new winston.transports.Stream({ stream: accessLogStream }),
        ],
    }),
);

app.use(
    expressWinston.errorLogger({
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.json(),
        ),
        transports: [
            new winston.transports.Console({
                format: winston.format.combine(
                    winston.format.colorize(),
                    winston.format.simple(),
                ),
            }),
            new winston.transports.Stream({
                stream: errorLogStream,
                level: "error",
            }),
        ],
    }),
);

// initializePassport(passport);

/* 
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, '..', 'public'))

app.use('*', (req, res, next) => {
    var file = path.join(__dirname, '..', 'public', req.originalUrl)
    if (file != null && file != '') {
        if (
            req.originalUrl.startsWith('/css') ||
            req.originalUrl.startsWith('/js') ||
            req.originalUrl.startsWith('/assets') ||
            req.originalUrl.startsWith('/favicon')
        ) {
            res.sendFile(file)
        } else {
            if (req.query.error && req.query.error != null) {
                res.render(
                    file,
                    { session: req.session, error: req.query.error },
                    (err, html) => {
                        if (err) {
                            if (err.message.startsWith('Failed to lookup view'))
                                error404(req, res, html)
                        } else {
                            res.send(html)
                        }
                    }
                )
            } else {
                res.render(file, { session: req.session }, (err, html) => {
                    if (err) {
                        if (err.message.startsWith('Failed to lookup view'))
                            error404(req, res, html)
                    } else {
                        res.send(html)
                    }
                })
            }
        }
    } else {
        next()
    }
}) */
