import { IsAlphanumeric, IsNotEmpty, Length } from "class-validator";
import { Column, Entity, JoinColumn, OneToOne } from "typeorm";
import { BaseEntity } from "./base";
import { User } from "./user";

@Entity()
export class BlogPost extends BaseEntity {
    @Column({ unique: true })
    @IsAlphanumeric()
    @IsNotEmpty()
    @Length(2, 50)
    title: string;

    @Column()
    @IsNotEmpty()
    @Length(1, 150)
    description: string;

    @Column()
    @IsNotEmpty()
    content: string;

    @OneToOne(() => User)
    @JoinColumn()
    author: User;

    @Column()
    @IsNotEmpty()
    category: string;

    /*
    title: { type: String, required: true },
    content: { type: String, required: true },
    author: { type: String, required: true },
    category: { type: String, required: false },
    description: { type: String, required: true },
    */
}
