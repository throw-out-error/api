import { ObjectId } from "mongodb";
import { Column, ObjectIdColumn, PrimaryGeneratedColumn } from "typeorm";

export abstract class BaseEntity {
    // @ObjectIdColumn() // MongoDB Only
    @PrimaryGeneratedColumn()
    id: string;

    @Column({ default: new Date().toDateString() })
    createdAt: string;
}
