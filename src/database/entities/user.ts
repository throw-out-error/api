import { IsEmail, IsNotEmpty } from "class-validator";
import { Column, Entity } from "typeorm";
import { BaseEntity } from "./base";

@Entity()
export class User extends BaseEntity {
    @Column({ unique: true })
    @IsEmail()
    @IsNotEmpty()
    username: string;

    @Column()
    name: string;

    @Column()
    password: string;

    @Column("text", { array: true })
    @IsNotEmpty()
    roles: string[];

    @Column({ nullable: true })
    avatar?: string;
}
