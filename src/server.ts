import { app } from "./app";
import "reflect-metadata";
import {
    createConnection,
    EntitySchema,
    Repository,
    useContainer,
} from "typeorm";
import { Container } from "typedi";
import { User } from "./database/entities/user";

const port = process.env.PORT || 8080;

async function main() {
    try {
        useContainer(Container);

        await createConnection({
            type: "postgres",
            host: process.env.DB_HOST || "localhost",
            port: parseInt(process.env.DB_PORT || "5432"),
            username: process.env.DB_USER || "postgres",
            password: process.env.DB_PASS,
            database: process.env.DB_NAME || "website",
            entities: [User],
            synchronize: true,
            logging: true,
        });
        console.log(`A connection to the database has been established.`);
        app.listen(port, () => {
            console.log(`Express server listening on 0.0.0.0:${port}`);
        });
    } catch (err) {
        console.error(
            `An error occurred while establishing a database connection.`,
        );
        return console.error(err);
    }
}
main();
