import { JsonController, Get } from "routing-controllers";

@JsonController("/")
export class IndexController {
    @Get("/")
    async getStatus() {
        return { message: "Hello, world!" };
    }
}
