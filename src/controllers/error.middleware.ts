import {
    Middleware,
    ExpressErrorMiddlewareInterface,
} from "routing-controllers";
import { Request, Response } from "express";

@Middleware({ type: "after" })
export class CustomErrorHandler implements ExpressErrorMiddlewareInterface {
    error(
        error: any,
        _: Request,
        response: Response,
        next: (err?: any) => any,
    ) {
        response.status(error.httpCode || 500).json({
            name: error.name,
            error: error.message,
            stack: process.env.DEBUG === "true" ? error.stack || null : null,
        });
    }
}
