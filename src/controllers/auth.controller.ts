import {
    JsonController,
    Get,
    Post,
    Delete,
    CurrentUser,
    BodyParam,
    Authorized,
} from "routing-controllers";

import { stripCredentials } from "../config";
import { User } from "../database/entities/user";
import jwt from "jsonwebtoken";
import Container from "typedi";
import { AuthService } from "../service/auth.service";

export const generateAccessToken = (user) =>
    jwt.sign(user, process.env.ACCESS_TOKEN_SECRET || "secret 1234", {
        expiresIn: "2h",
    });

@JsonController("/auth")
export class AuthController {
    authService: AuthService;

    constructor() {
        this.authService = Container.get(AuthService);
    }

    @Get("/profile")
    @Authorized()
    async getProfile(@CurrentUser({ required: true }) user: User) {
        return { user: stripCredentials(user) };
    }

    @Post("/user/create")
    async createUser(
        @BodyParam("username", { required: true, validate: true })
        username: string,
        @BodyParam("name", { required: true, validate: true })
        name: string,
        @BodyParam("password", { required: true, validate: true })
        password: string,
    ) {
        return this.authService.createUser(username, name, password);
    }

    @Post("/user")
    async login(
        @BodyParam("username", { required: true, validate: true })
        username: string,
        @BodyParam("password", { required: true, validate: true })
        password: string,
    ) {
        return await this.authService.login(username, password);
    }

    @Post("/token")
    refreshToken(@BodyParam("token") token: string) {
        return this.authService.refreshToken(token);
    }

    @Post("/logout")
    logout(@BodyParam("token") bodyToken: string) {
        return this.authService.logout(bodyToken);
    }
}
