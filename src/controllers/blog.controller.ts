import {
    JsonController,
    Get,
    Post,
    Delete,
    CurrentUser,
    BodyParam,
    Authorized,
    QueryParam,
} from "routing-controllers";

import { stripCredentials } from "../config";
import { User } from "../database/entities/user";
import jwt from "jsonwebtoken";
import Container from "typedi";
import { AuthService } from "../service/auth.service";
import { BlogPost } from "../database/entities/post";

export const generateAccessToken = (user) =>
    jwt.sign(user, process.env.ACCESS_TOKEN_SECRET || "secret 1234", {
        expiresIn: "2h",
    });

@JsonController("/blog")
export class BlogController {
    authService: AuthService;

    constructor() {
        this.authService = Container.get(AuthService);
    }

    @Get("/posts")
    async getPosts(
        @QueryParam("search") search?: string,
        @QueryParam("category") category?: string,
        @QueryParam("date") date?: string,
    ) {
        let posts: BlogPost[] = [];
        if (search)
            posts = posts.filter(
                (p) => p.title != "" && p.title.toLowerCase().includes(search),
            );
        if (date)
            posts = posts.filter(
                (p) =>
                    p.createdAt.toLocaleString().split(",")[0] ===
                    new Date(date).toLocaleString().split(",")[0],
            );
        if (category) posts = posts.filter((p) => p.category === category);
        return posts;
    }
}
